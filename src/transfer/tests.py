from multiprocessing import Process
from decimal import Decimal
import mock

from django.db import transaction, connection
import django.db.utils
from django.test import TransactionTestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from .models import Wallet
from .factories import WalletFactory
from . import errors


class WalletConstraintsTestCase(TransactionTestCase):
    """Проверка ограничений и переводов в слое персистентности."""

    @staticmethod
    def test_balance_can_be_zero():
        """Баланс может быть нулевым."""
        WalletFactory(balance='0')

    def test_balance_can_be_positive(self):
        """Баланс может быть положительным."""
        w = WalletFactory(balance='0.01')
        w.refresh_from_db()
        self.assertEqual(w.balance, Decimal('0.01'))

    def test_balance_can_not_be_negative(self):
        """Баланс не может быть отрицательным."""
        self.assertRaises(django.db.utils.IntegrityError, WalletFactory, balance='-0.01')

    def test_transfer_positive(self):
        """Перевод денег, простейший случай."""
        w1 = WalletFactory(balance=100)
        w2 = WalletFactory(balance=50)
        journal = Wallet.objects.transfer(
            source=w1,
            destination=w2,
            amount=30
        )

        self.assertEqual(w1.outgoing_journals.all().count(), 1)
        self.assertEqual(w2.incoming_journals.all().count(), 1)
        self.assertEqual(w1.outgoing_journals.all()[0], journal)
        self.assertEqual(journal.amount, 30)
        w1.refresh_from_db()
        self.assertEqual(w1.balance, 100 - 30)
        w2.refresh_from_db()
        self.assertEqual(w2.balance, 50 + 30)

    def test_transfer_from_none(self):
        """Начисленеие денег из пустоты."""
        w = WalletFactory(balance=5)
        Wallet.objects.transfer(
            source=None,
            destination=w,
            amount=20
        )
        w.refresh_from_db()
        self.assertEqual(w.balance, 25)

    def test_transfer_negative(self):
        """Не получится перевести отрицательную сумму."""
        w1 = WalletFactory(balance=10)
        w2 = WalletFactory(balance=20)
        self.assertRaises(
            errors.NegativeTransaction,
            Wallet.objects.transfer,
            source=w1,
            destination=w2,
            amount=-5
        )

    def test_not_enough_money(self):
        """Не получится перевести больше остатка."""
        w1 = WalletFactory(balance=10)
        w2 = WalletFactory(balance=20)
        with mock.patch('transfer.models.WalletManager._raise_no_enough_money_exception'):
            self.assertRaises(
                django.db.utils.IntegrityError,
                Wallet.objects.transfer,
                source=w1,
                destination=w2,
                amount=Decimal('10.01')
            )
        # журнал операции не создаётся в случае ошибки
        self.assertEqual(w1.outgoing_journals.all().count(), 0)
        self.assertEqual(w2.incoming_journals.all().count(), 0)

    def test_transfer_accuracy_below_supported(self):
        """Запрещены переводы за пределами точности хранения данных."""
        w1 = WalletFactory(balance=10)
        w2 = WalletFactory(balance=20)
        self.assertRaises(
            AssertionError,
            Wallet.objects.transfer,
            source=w1,
            destination=w2,
            amount=Decimal('0.001')
        )

    def test_concurrency(self):
        """Параллельные запросы из нескольких процессов."""
        PROCESS_COUNT = 100
        PORTION = Decimal('0.1')

        with transaction.atomic():
            w1 = WalletFactory(balance=10)
            w2 = WalletFactory(balance=20)

        def transfer_money():
            # закрываем файловый дескриптор соединения, наследованного от родителя
            # и открываем новый, чтобы не было неожиданностей
            connection.connect()

            while True:
                try:
                    Wallet.objects.transfer(
                        source=w1,
                        destination=w2,
                        amount=PORTION
                    )
                except errors.TransferTemporarilyUnavailable:
                    pass
                else:
                    break

        process_pool = [Process(target=transfer_money) for i in range(PROCESS_COUNT)]
        for p in process_pool:
            p.start()

        for p in process_pool:
            p.join()

        w1.refresh_from_db()
        self.assertEqual(w1.balance, 10 - PROCESS_COUNT * PORTION)
        w2.refresh_from_db()
        self.assertEqual(w2.balance, 20 + PROCESS_COUNT * PORTION)


class ApplicationServerTest(APITestCase):
    """Тестирование на уровне бизнес-логики."""
    def test_create_wallet(self):
        """Создание кошелька."""
        url = reverse('wallet-create')
        self.assertEqual(Wallet.objects.count(), 0)
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Wallet.objects.count(), 1)

    def test_refill_positive_case(self):
        """Пополнение счёта: успех."""
        wallet = WalletFactory(balance=1)
        url = reverse('transfer-money')
        response = self.client.post(url, data={'source': '', 'destination': wallet.id, 'amount': 1})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.content)
        wallet.refresh_from_db()
        self.assertEqual(wallet.balance, 2)

    def test_refill_negative_case(self):
        """Пополнение счёта: ошибка."""
        wallet = WalletFactory(balance=1)
        url = reverse('transfer-money')
        response = self.client.post(url, data={'source': '', 'destination': wallet.id, 'amount': -1})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.content)
        wallet.refresh_from_db()
        self.assertEqual(wallet.balance, 1)

    def test_transfer_positive_case(self):
        """Перевод денег: успех."""
        wallet1 = WalletFactory(balance=1)
        wallet2 = WalletFactory(balance=1)
        url = reverse('transfer-money')
        response = self.client.post(url, data={'source': wallet1.id, 'destination': wallet2.id, 'amount': 1})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.content)
        wallet1.refresh_from_db()
        self.assertEqual(wallet1.balance, 0)
        wallet2.refresh_from_db()
        self.assertEqual(wallet2.balance, 2)

    def test_transfer_negative_case(self):
        """Перевод денег: ошибка."""
        wallet1 = WalletFactory(balance=1)
        wallet2 = WalletFactory(balance=1)
        url = reverse('transfer-money')
        response = self.client.post(url, data={'source': wallet1.id, 'destination': wallet2.id, 'amount': 10})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.content.decode('utf8'))
        wallet1.refresh_from_db()
        self.assertEqual(wallet1.balance, 1)
        wallet2.refresh_from_db()
        self.assertEqual(wallet2.balance, 1)
