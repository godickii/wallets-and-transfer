from django.urls import path
from . import views

urlpatterns = [
    path('create/', views.CreateWalletView.as_view(), name='wallet-create'),
    path('transfer/', views.TransferView.as_view(), name='transfer-money'),
]
