from decimal import Decimal

from django.db import models, transaction, connection
import django.db.utils

from . import errors

MAX_BALANCE_DIGITS = 20  # количество хранимых цифр в остатке денег


class WalletJournal(models.Model):
    """История изменений кошельков."""
    created_at = models.DateTimeField('Создано', auto_now_add=True)
    source = models.ForeignKey('Wallet', null=True, related_name='outgoing_journals', on_delete=models.PROTECT)
    destination = models.ForeignKey('Wallet', related_name='incoming_journals', on_delete=models.PROTECT)
    amount = models.DecimalField('Сумма перевода', max_digits=MAX_BALANCE_DIGITS, decimal_places=2)


class WalletManager(models.Manager):
    """Менеджер кошельков."""
    POSTGRESQL_CONCURRENT_UPDATE_ERROR = 'could not serialize access due to concurrent update'

    def _raise_no_enough_money_exception(self):
        # Зачем? См. тесты.
        raise errors.NotEnoughtMoney()

    @transaction.atomic()
    def transfer(self, source, destination, amount):
        """Сделать перевод с кошелька source на кошелёк destination в количестве amount.

        @param source: кошелёк источника или None
        @param destination: кошелёк назначения
        @param amount: количество денег
        @return WalletJournal
        """
        assert source is None or isinstance(source, Wallet)
        assert isinstance(destination, Wallet)
        assert isinstance(amount, (int, Decimal))
        assert source != destination
        if isinstance(amount, Decimal):
            assert amount.as_tuple().exponent >= -2
        if amount < 0:
            raise errors.NegativeTransaction()
        assert amount >= 0

        cursor = connection.cursor()
        cursor.execute('SET TRANSACTION ISOLATION LEVEL READ COMMITTED')

        row_for_locks = source and (source.pk, destination.pk) or (destination.pk,)

        try:
            # выполняем QuerySet
            # https://docs.djangoproject.com/en/3.0/ref/models/querysets/#when-querysets-are-evaluated
            # при одновременном блокировании ресурсов не будет мёртвой блокировки
            wallets = list(self.filter(pk__in=row_for_locks).select_for_update())
        except django.db.utils.OperationalError as e:
            # СУБД обеспечения целостности может как ожидать освобождения блокировки,
            # так и выдать ошибку
            # Postgresql в случае repeatable read или выше возвращет ошибку:
            # https://pganalyze.com/docs/log-insights/app-errors/U138
            # https://habr.com/ru/company/postgrespro/blog/442804/
            # https://www.postgresql.org/message-id/1186781651.11237.233.camel%40linda.lfix.co.uk
            if self.POSTGRESQL_CONCURRENT_UPDATE_ERROR in e.args[0].lower():
                raise errors.TransferTemporarilyUnavailable from e
            else:
                raise
        else:
            for wallet in wallets:
                if wallet == source:
                    if wallet.balance < amount:
                        self._raise_no_enough_money_exception()
                    self.filter(pk=wallet.pk).update(balance=models.F('balance') - amount)
                if wallet == destination:
                    self.filter(pk=wallet.pk).update(balance=models.F('balance') + amount)

            return WalletJournal.objects.create(
                source=source,
                destination=destination,
                amount=amount
            )


class Wallet(models.Model):
    """Кошелёк."""
    balance = models.DecimalField('Остаток денег', max_digits=MAX_BALANCE_DIGITS, decimal_places=2)

    objects = WalletManager()

    class Meta:
        constraints = [
            models.CheckConstraint(check=models.Q(balance__gte='0'), name='wallet_balance_non_negative'),
        ]
