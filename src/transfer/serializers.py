from rest_framework import serializers, status
from rest_framework.exceptions import ValidationError

from . import models, errors


class WalletCreateSerializer(serializers.ModelSerializer):
    """Создание кошелька."""
    id = serializers.ReadOnlyField()

    def validate(self, data):
        data['balance'] = 0
        return data

    class Meta:
        model = models.Wallet
        fields = ('id', )


class TransferMoneySerializer(serializers.Serializer):
    """Перевод денег."""
    source = serializers.PrimaryKeyRelatedField(queryset=models.Wallet.objects.all(), allow_null=True)
    destination = serializers.PrimaryKeyRelatedField(queryset=models.Wallet.objects.all())
    amount = serializers.DecimalField(max_digits=models.MAX_BALANCE_DIGITS, decimal_places=2)

    def save(self):
        try:
            models.Wallet.objects.transfer(
                source=self.validated_data['source'],
                destination=self.validated_data['destination'],
                amount=self.validated_data['amount'],
            )
        except errors.UserError as e:
            # Это всегда дискуссионный вопрос, какой HTTP-код мы используем, если
            # запрос корректный синтаксически и семантически, но его нельзя сейчас выполнить
            # у нас пусть будет 400.
            raise ValidationError({e.__class__.__name__: e.message}) from e
