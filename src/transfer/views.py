from rest_framework import generics

from . import serializers


class CreateWalletView(generics.CreateAPIView):
    serializer_class = serializers.WalletCreateSerializer


class TransferView(generics.CreateAPIView):
    serializer_class = serializers.TransferMoneySerializer
