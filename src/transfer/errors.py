class BaseTransferException(Exception):
    pass


class TransferTemporarilyUnavailable(BaseTransferException):
    """Невозможно заблокировать строку таблицы."""


class UserError(BaseTransferException):
    """Ошибка, которую нужно прокинуть клиенту."""


class NotEnoughtMoney(UserError):
    message = "Недостаточно денег."


class NegativeTransaction(UserError):
    message = "Нельзя передать отрицательную сумму."
