import factory

from .models import Wallet


class WalletFactory(factory.DjangoModelFactory):
    """Фабрика для push_channel."""

    class Meta:
        """Метаданные фабрики."""

        model = Wallet
