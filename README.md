# Тестовое задание

## Требования

 * Каждый клиент в системе имеет один "кошелек", содержащий денежные средства
 * Сохраняется информация о кошельке и остатке средств на нем.
 * Клиенты могут делать друг другу денежные переводы
 * Сохраняется информация о всех операциях на кошельке клиента.

## Ресурсы API

 * создание клиента с кошельком
 * перевод денежных средств с одного кошелька на другой
 * зачисление денежных средств на кошелек клиента (частный случай предыдущей операции)

## Ограничения и особенности

 * нет комиссий
 * нет аутентификации и авторизации
 * одна валюта
 * отсутствие DRF browsable API
 * овердрафт недопустим
 * сохраняем только успешно проведённые операции с кошельками
 * PEP8 предполагает ограничение строки в 80 символов, на практике сейчас используют ограничения в 100, 110 (flake8) или 120 (разные IDE) символов. Здесь 120

## Хранение данных

Реляционная СУБД, ибо проверенная технология с поддержкой ACID, блокировок. Postgresql - хорошая СУБД с открытым исходным кодом.

## Установка

```
$ pyenv local 3.8.2  
$ python -m venv venv && source venv/bin/activate  
$ pip install --upgrade pip  
$ pip install -r requirements.txt

$ psql -U postgres  
# create user money_transfer;  
# alter user money_transfer with password '1';  
# create database money_transfer;  
# alter user money_transfer with superuser;  
# exit

$ src/manage.py migrate  
```

## Локальный запуск

`$ src/manage.py runserver`

## Примеры запросов

### Создание кошелька

```
$ curl -H 'Content-type: application/json' -X POST http://localhost:8000/create/
HTTP 201 {"id":123}
```

### Зачисление

```
curl -H 'Content-type: application/json' -X POST -d '{
    "source": null,
    "destination": 1,
    "amount": "0.01"
}' http://localhost:8000/transfer/
HTTP 201
```

### Перевод

```
$ curl -H 'Content-type: application/json' -X POST -d '{
    "source": 1,
    "destination": 2,
    "amount": "0.01"
}' http://localhost:8000/transfer/
HTTP 201
```

Или невозможный перевод:
```
$ curl -H 'Content-type: application/json' -X POST -d '{
    "source": 1,
    "destination": 2,
    "amount": "0.01"
}' http://localhost:8000/transfer/
HTTP 400 {"NegativeTransaction":"Нельзя передать отрицательную сумму."}
```

## Тесты

Тесты разделены на два этапа:

 * Слой персистентности.
 * Слой бизнес-логики + слой персистентности.

Канонично тестируют слои по-отдельности, но при этом в случае изменения контрактов взаимодействия между слоями тесты проходят, а всё вместе ломается.

Запуск тестов:

```
$ src/manage.py test --noinput --verbosity=2 transfer
...
test_create_wallet (transfer.tests.ApplicationServerTest)
Создание кошелька. ... ok
test_refill_negative_case (transfer.tests.ApplicationServerTest)
Пополнение счёта: ошибка. ... ok
test_refill_positive_case (transfer.tests.ApplicationServerTest)
Пополнение счёта: успех. ... ok
test_transfer_negative_case (transfer.tests.ApplicationServerTest)
Перевод денег: ошибка. ... ok
test_transfer_positive_case (transfer.tests.ApplicationServerTest)
Перевод денег: успех. ... ok
test_balance_can_be_positive (transfer.tests.WalletConstraintsTestCase)
Баланс может быть положительным. ... ok
test_balance_can_be_zero (transfer.tests.WalletConstraintsTestCase)
Баланс может быть нулевым. ... ok
test_balance_can_not_be_negative (transfer.tests.WalletConstraintsTestCase)
Баланс не может быть отрицательным. ... ok
test_concurrency (transfer.tests.WalletConstraintsTestCase)
Параллельные запросы из нескольких процессов. ... ok
test_not_enough_money (transfer.tests.WalletConstraintsTestCase)
Не получится перевести больше остатка. ... ok
test_transfer_accuracy_below_supported (transfer.tests.WalletConstraintsTestCase)
Запрещены переводы за пределами точности хранения данных. ... ok
test_transfer_from_none (transfer.tests.WalletConstraintsTestCase)
Начисленеие денег из пустоты. ... ok
test_transfer_negative (transfer.tests.WalletConstraintsTestCase)
Не получится перевести отрицательную сумму. ... ok
test_transfer_positive (transfer.tests.WalletConstraintsTestCase)
Перевод денег, простейший случай. ... ok
```

